import React, { Component } from "react";
import  RequestViewStatusbar  from "./RequestViewStatusbar";
import RequestViewForm from "./RequestViewForm";

class RequestView extends Component {
  render() {
    return (
      <div>
        <RequestViewStatusbar />
        <RequestViewForm/>
      </div>
    );
  }
}

export default RequestView;
